package cn.pheker.utils;

import cn.pheker.utils.interfaces.ElapsedTimeCallback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
<<<<<<< HEAD
 * @author cn.pheker
 * @date ${DATE} ${TIME}
 * @email 1176479642@qq.com
 * @desc calculate elapsed time and format
 *
 **/


/** <pre>
 * author    cn.pheker
 * date      2018/11/10 16:48
 * email     1176479642@qq.com
 * desc      calculate elapsed time and format
 *
 * </pre>
 */
public class Time {
    
    public static String format(long time) {
        if (time < 0) {
            time = -time;
        }
        long ms = time % 1000;
        time /= 1000;
        long s = time % 60;
        time /= 60;
        long m = time % 60;
        time /= 60;
        long h = time % 24;
        long d = time / 24;
        return String.format("%dd %dh %dm %ds %dms", d, h, m, s, ms);
    }
    
    public static String format(Date startTime, Date endTime) {
        return format(endTime.getTime() - startTime.getTime());
    }
    
    /**
     * 利用CGLIB代理并计算耗时
     * @param target    委托类,final方法不会代理
     * @param <T>       委托类泛型
     * @return          委托类
     */
    public static <T> T proxy(T target) {
        return (T) proxy(target.getClass());
    }
    
     /**
     * 利用CGLIB代理并计算耗时
     * @param target    委托类,final方法不会代理
     * @param <T>       委托类泛型
     * @return          委托类
     */
    public static <T> T proxy(T target, ElapsedTimeCallback elapsedTimeCb) {
        return (T) proxy(target.getClass(), elapsedTimeCb);
    }
    
     /**
     * 利用CGLIB代理并计算耗时
     * @param targetClzz    委托类class,final方法不会代理
     * @param <T>           委托类泛型
     * @return              委托类
     */
    public static <T> T proxy(Class<T> targetClzz) {
        return proxy(targetClzz, (ElapsedTimeCallback) null);
    }
    /**
     * 利用CGLIB代理并计算耗时
     * @param targetClzz    委托类class,final方法不会代理
     * @param <T>           委托类泛型
     * @param elapsedTimeCb 耗时回调
     * @return              委托类
     */
    public static <T> T proxy(Class<T> targetClzz, ElapsedTimeCallback elapsedTimeCb) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(targetClzz);
        ElapsedTimeAop aop = new ElapsedTimeAop(targetClzz.getCanonicalName(), elapsedTimeCb);
        ElapsedTimeCglibProxyHandler callback = new ElapsedTimeCglibProxyHandler(aop);
        enhancer.setCallback(callback);
        return (T) enhancer.create();
    }
    
    /**
     * 利用JDK代理并计算耗时
     * @param target 委托类
     * @param interfaceClzz   委托类的接口,必须是public
     * @param <T>    接口泛型
     * @return       委托类
     */
    public static <T> T proxy(Object target, Class<T> interfaceClzz) {
        return proxy(target, interfaceClzz, null);
    }
    
    /**
     * 利用jdk动态代理进行计算耗时
     *
     * @param target 委托类
     * @param interfaceClzzs  委托类的接口,必须是public
     * @param <T>    接口泛型
     * @return 委托类
     */
    public static <T> T proxy(Object target, Class<T>[] interfaceClzzs) {
        return proxy(target, interfaceClzzs, null);
    }
    
    /**
     * 利用jdk动态代理进行计算耗时
     *
     * @param target         委托类
     * @param interfaceClzzs 委托类的接口,必须是public
     * @param <T>            接口泛型
     * @param elapsedTimeCb 耗时回调
     * @return 委托类
     */
    public static <T> T proxy(Object target, Class<T>[] interfaceClzzs,
                              ElapsedTimeCallback elapsedTimeCb) {
        for (Class interfaceClzz :interfaceClzzs) {
            if (!interfaceClzz.isInterface()) {
                throw new RuntimeException(String.format("%s is not a interface", interfaceClzz.getName()));
            }
        }
        ElapsedTimeProxyHandler handler = new ElapsedTimeProxyHandler(target,
                new ElapsedTimeAop(target.getClass().getCanonicalName(), elapsedTimeCb));
        return (T) Proxy.newProxyInstance(target.getClass().getClassLoader(),
                interfaceClzzs, handler);
    }
    
    /**
     * 利用JDK代理并计算耗时
     * @param target 委托类
     * @param interfaceClzz   委托类的接口,必须是public
     * @param <T>    接口泛型
     * @param elapsedTimeCb 耗时回调
     * @return 委托类
     */
    public static <T> T proxy(Object target, Class<T> interfaceClzz, ElapsedTimeCallback elapsedTimeCb) {
        if (!interfaceClzz.isInterface()) {
            throw new RuntimeException(String.format("%s is not a interface", interfaceClzz.getName()));
        }
         ElapsedTimeProxyHandler handler = new ElapsedTimeProxyHandler(target,
                new ElapsedTimeAop(target.getClass().getCanonicalName(), elapsedTimeCb));
        return (T) Proxy.newProxyInstance(target.getClass().getClassLoader(),
                new Class[]{interfaceClzz}, handler);
    }
    
    /**
     * aop接口
     */
    interface AopAware {
        
        /**
         * 前置
         */
        void before(Method method);
        
        /**
         * 后置
         */
        void after(Method method);
    
       
    
    }
    
    static class ElapsedTimeAop implements AopAware {
        String clazzName;
        ElapsedTimeCallback elapsedTimeCallback;
        
        long startTime;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
        public ElapsedTimeAop(String clazzName) {
            this.clazzName = clazzName;
        }
    
        public ElapsedTimeAop(String clazzName, ElapsedTimeCallback elapsedTimeCallback) {
            this.clazzName = clazzName;
            this.elapsedTimeCallback = elapsedTimeCallback;
        }
        
        @Override
        public void before(Method method) {
            startTime = System.currentTimeMillis();
            String date = sdf.format(new Date(startTime));
            System.out.println(String.format("[%s-%s]: %s",clazzName,method.getName(), date));
        }
        
        @Override
        public void after(Method method) {
            long endTime = System.currentTimeMillis();
            String date = sdf.format(new Date(startTime));
            long elapsedTime = endTime - startTime;
            String elapsedTimeFormatted = Time.format(elapsedTime);
            String output = String.format("[%s-%s]: %s => %s",
                    clazzName, method.getName(), date, elapsedTimeFormatted);
            if (elapsedTimeCallback != null) {
                elapsedTimeFormatted = elapsedTimeCallback.callback(elapsedTime, elapsedTimeFormatted);
                String newFormatted = String.format("[%s-%s]: %s => %s",
                        clazzName, method.getName(), date, elapsedTimeFormatted);
                output = newFormatted == null ? output : newFormatted;
            }
            System.out.println(output);
        }
    }
    
    /**
     * 耗时代理处理器
     */
    static class ElapsedTimeProxyHandler implements InvocationHandler {
        
        private final Object target;
        private final AopAware aop;
        
        ElapsedTimeProxyHandler(Object target, AopAware aop) {
            this.target = target;
            this.aop = aop;
        }
        
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            aop.before(method);
            Object result = method.invoke(this.target, args);
            aop.after(method);
            return result;
        }
    }
    
    static class ElapsedTimeCglibProxyHandler implements MethodInterceptor {
       private AopAware aop;
    
        public ElapsedTimeCglibProxyHandler(AopAware aop) {
            this.aop = aop;
        }
    
        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            
            aop.before(method);
            Object result = methodProxy.invokeSuper(o, objects);
            aop.after(method);
            return result;
        }
    }
}