package cn.pheker.utils.interfaces;

/**
 * <pre>
 * author    cn.pheker
 * date      2018/11/10 18:41
 * email     1176479642@qq.com
 * desc      计算耗时的回调函数,可以用其获取耗时或重新格式化
 *
 * </pre>
 */
public interface ElapsedTimeCallback {
    String callback(long elapsedTime, String elapsedTimeFormatted);
}
