package cn.pheker.study.proxy.service;

import java.io.Serializable;

/**
 * <pre>
 * @author    cn.pheker
 * @date      2018/11/10 18:06
 * @email     1176479642@qq.com
 * @desc
 *
 * </pre>
 */
public interface Service extends Serializable{
    
    void serve();
    
}