package cn.pheker.study.proxy.service.impl;

import cn.pheker.study.proxy.service.BookService;

import java.util.concurrent.TimeUnit;

/**
 * BookService实现类
 */
public class BookBookServiceImpl implements BookService {
    
    @Override
    public void serve() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            System.out.println("Books is on sale.");
        }
    }
}
