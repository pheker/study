package cn.pheker.study.proxy.jdk;

import cn.pheker.study.proxy.service.impl.BookBookServiceImpl;
import cn.pheker.study.proxy.service.BookService;
import cn.pheker.utils.Time;

/**
 * @author   cn.pheker
 * @date     2018/11/10 15:09
 * @email    1176479642@qq.com
 * @desc     jdk动态代理测试
 * desc
 *
 */
public class JdkProxy {
    
    
    public static void main(String[] args) {
        BookBookServiceImpl bookServiceImpl = new BookBookServiceImpl();
        BookService service = Time.proxy(bookServiceImpl, BookService.class);
        service.serve();
    }
}


