package cn.pheker.study.proxy.cglib;

import cn.pheker.study.proxy.service.BookService;
import cn.pheker.study.proxy.service.impl.BookBookServiceImpl;
import cn.pheker.utils.Time;

/**
 * @author cn.pheker
 * @date   2018/11/11 13:59
 * @email  1176479642@qq.com
 * @desc   CGLIB动态代理测试
 */
public class CglibProxy {
    
    public static void main(String[] args) {
        BookService proxy = Time.proxy(BookBookServiceImpl.class);
        proxy.serve();
    }
}
